import { createApp } from 'vue';
import Dev from './serve.vue';
import HtComponents from '@/entry.esm';
const app = createApp(Dev);


import './theme/main.scss';


app.use(HtComponents);

app.mount('#app');
